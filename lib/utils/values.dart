import 'package:google_maps_flutter/google_maps_flutter.dart';

class BuildingLocations{

  static const LatLng PACKARD_LOCATION = LatLng(40.607801, -75.379196);
  static const LatLng FRITZ_LOCATION = LatLng(40.6080175, -75.3761864);
  static const LatLng WHITAKER_LOCATION = LatLng(40.6089908, -75.3763878);
  static const LatLng BUILDING_C_LOCATION = LatLng(40.600977, -75.362498);
  }

