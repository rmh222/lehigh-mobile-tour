import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:untitled/styles/colors.dart';
import 'package:untitled/LandingPages/fritzInfo.dart';
import 'package:untitled/LandingPages/packardInfo.dart';
import 'package:untitled/pin_info.dart';
import 'package:untitled/map_pin_information.dart';
import 'dart:async';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:untitled/LandingPages/stepsInfo.dart';
import 'package:untitled/LandingPages/whitakerInfo.dart';
import 'package:untitled/utils/values.dart';
import 'LandingPages/buildingcinfo.dart';

class Map extends StatefulWidget {
  @override
  _Map createState() => _Map();
}

class _Map extends State<Map> with AutomaticKeepAliveClientMixin<Map> {
@override
bool get wantKeepAlive => true;
  late BitmapDescriptor mapMarker;
  late GoogleMapController mapController;
  late String color;
  Set<Marker> _markers = {};
  static const double CAMERA_ZOOM = 14;
  static const double CAMERA_TILT = 0;
  static const double CAMERA_BEARING = 0;
  Completer<GoogleMapController> _controller = Completer();
  Set<Polyline> _polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  late Position  _currentPosition;
  String _currentAddress = '';
  final startAddressController = TextEditingController();
  final destinationAddressController = TextEditingController();
  final startAddressFocusNode = FocusNode();
  final desrinationAddressFocusNode = FocusNode();

  String _startAddress = '';
  String _destinationAddress = '';
  String? _placeDistance;
  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
    setMarkerColor();
    setSourceAndDestinationIcons();
    }

void setMarkerColor() async {
  mapMarker =  BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed);
  color = "red";
}

_getAddress() async {
  try {
    List<Placemark> p = await placemarkFromCoordinates(
        _currentPosition.latitude, _currentPosition.longitude);
    Placemark place = p[0];
    setState(() {
      _currentAddress = "${place.name}, ${place.locality}, ${place.postalCode}, ${place.country}";
      startAddressController.text = _currentAddress;
      _startAddress = _currentAddress;
    });
  } catch (e) {
    print(e);
  }
}

  void onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(Utils.mapStyles);
    _controller.complete(controller);
    setMapPins();
    setPolylines();
  }


  setPolylines() async
  {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
           'AIzaSyAYWjfg6nDnCHvorZhEkgEl01iEN99jT1g',
        PointLatLng(BuildingLocations.PACKARD_LOCATION.latitude, BuildingLocations.PACKARD_LOCATION.longitude),
        PointLatLng(BuildingLocations.FRITZ_LOCATION.latitude, BuildingLocations.FRITZ_LOCATION.longitude));

    if (result != 0) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });

      setState(() {
        Polyline polyline = Polyline(
            polylineId: PolylineId("poly"),
            color: Color.fromARGB(255, 40, 122, 198),
            points: polylineCoordinates
        );
        _polylines.add(polyline);
      });
    }
  }


  void setMapPins() {
    _markers.add(
        Marker(
            markerId: MarkerId('packardPin'),
            position: BuildingLocations.PACKARD_LOCATION,
            icon:mapMarker,
            onTap: () {

              setState(() {
                if(color == "red") {
                  currentlySelectedPin = sourcePinInfo;
                  pinPillPosition = 0;
                  mapMarker = BitmapDescriptor.defaultMarkerWithHue(
                      BitmapDescriptor.hueBlue);
                  color = "blue";
                  print("blue");
                }
                else{
                  mapMarker = BitmapDescriptor.defaultMarkerWithHue(
                      BitmapDescriptor.hueRed);
                  color = "red";
                }
              });
            },
           // icon: sourceIcon
        ));
    sourcePinInfo = PinInformation(
        locationName: "Packard Laboratory",
        location: BuildingLocations.PACKARD_LOCATION,
        pinPath: "assets/packard2.jpg",
        avatarPath: "assets/packard2.jpg",
        labelColor: AppColors.lehighBrown01,
        routeName: PackardInfo(1),
    );
    // add the destination marker to the list of markers
    _markers.add(Marker(
        markerId: MarkerId('fritzPin'),
        position: BuildingLocations.FRITZ_LOCATION,
        onTap: () {
          setState(() {
            currentlySelectedPin = destinationPinInfo;
            pinPillPosition = 0;
          });
        },
      //  icon: destinationIcon
    ));
    destinationPinInfo = PinInformation(
        locationName: "Fritz Laboratory",
        location: BuildingLocations.FRITZ_LOCATION,
        pinPath: "assets/fritzlab.jpg",
        avatarPath: "assets/fritzlab.jpg",
        labelColor: AppColors.lehighBrown01,
      routeName: FritzInfo(2),
    );



     _markers.add(Marker(
        markerId: MarkerId('whitakerPin'),
        position: BuildingLocations.WHITAKER_LOCATION,
        onTap: () {
          setState(() {
            currentlySelectedPin = whitakerInfo;
            pinPillPosition = 0;
          });
        },
      //  icon: destinationIcon
    ));
    whitakerInfo = PinInformation(
        locationName: "Whitaker Laboratory",
        location: BuildingLocations.WHITAKER_LOCATION,
        pinPath: "assets/fritzlab.jpg",
        avatarPath: "assets/fritzlab.jpg",
        labelColor: AppColors.lehighBrown01,
      routeName: WhitakerInfo(3),
    );


    //  steps location
//
// _markers.add(Marker(
//         markerId: MarkerId('stepsPin'),
//         position: BuildingLocations.STEPS_LOCATION,
//         onTap: () {
//           setState(() {
//             currentlySelectedPin = stepsInfo;
//             pinPillPosition = 0;
//           });
//         },
//       //  icon: destinationIcon
//     ));
//     stepsInfo = PinInformation(
//         locationName: "STEPS",
//         location: BuildingLocations.STEPS_LOCATION,
//         pinPath: "assets/fritzlab.jpg",
//         avatarPath: "assets/fritzlab.jpg",
//         labelColor: AppColors.lehighBrown01,
//       routeName: StepsInfo(4),
//     );
//


   _markers.add(Marker(
        markerId: MarkerId('buildingcPin'),
        position: BuildingLocations.BUILDING_C_LOCATION,
        onTap: () {
          setState(() {
            currentlySelectedPin = buildingCinfo;
            pinPillPosition = 0;
          });
        },
      //  icon: destinationIcon
    ));
    buildingCinfo = PinInformation(
        locationName: "Building C",
        location: BuildingLocations.BUILDING_C_LOCATION,
        pinPath: "assets/fritzlab.jpg",
        avatarPath: "assets/fritzlab.jpg",
        labelColor: AppColors.lehighBrown01,
      routeName: BuildingCInfo(5),
    );

  }

  double pinPillPosition = -100;
  PinInformation currentlySelectedPin = PinInformation(
      pinPath: '',
      avatarPath: '',
      location: LatLng(0, 0),
      locationName: '',
    labelColor: AppColors.lehighBrown01,
    routeName: FritzInfo(2),
);
  late PinInformation sourcePinInfo;
  late PinInformation destinationPinInfo;
  late PinInformation whitakerInfo;
  late PinInformation stepsInfo;

  late PinInformation buildingCinfo;

  void setSourceAndDestinationIcons() async {
  }


_getCurrentLocation() async {
  await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
      .then((Position position) async {
    setState(() {
      _currentPosition = position;
      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(position.latitude, position.longitude),
            zoom: 18.0,
          ),
        ),
      );
    });
    await _getAddress();
  }).catchError((e) {
    print(e);
  });
}
  @override
  Widget build(BuildContext context) {
    CameraPosition initialLocation = CameraPosition(
        zoom: CAMERA_ZOOM,
        bearing: CAMERA_BEARING,
        tilt: CAMERA_TILT,
        target: LatLng(40.605986, -75.372868)
    );

  return Scaffold(
      appBar: AppBar(
        elevation:0,
        backgroundColor: Colors.white,
        title: Text("Campus Map", style: TextStyle(fontFamily: "Merriweather", fontSize: 20, color: AppColors.lehighBrown01),),
      ),
        body: Stack(
            children: <Widget>[
              GoogleMap(
                myLocationEnabled: true,
                onMapCreated: onMapCreated,
                initialCameraPosition: initialLocation,
                markers: _markers,
                onTap: (LatLng location) {
                  setState(() {
                    pinPillPosition = -100;
                  });
                },
              ),
              MapPinPillComponent(
                  pinPillPosition: pinPillPosition,
                  currentlySelectedPin: currentlySelectedPin
              )
            ])
    );
  }
}

class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';
}

