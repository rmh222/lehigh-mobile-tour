import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:untitled/styles/colors.dart';

class Carousel extends StatelessWidget {
  CarouselController buttonCarouselController = CarouselController();

  final List<String> feedbackList = [
    "I love having the freedom of choosing which part of computer science I want to go into.",
    "I think it's neat to learn how all of our modern electronics work, and use that knowledge to create something that seems really impressive on my own"
    ,
    "Mechanical engineering is a very diverse and hands-on field. If you like building things and using your skills to come up with clever solutions to problems, you will enjoy mechanical engineering. It certainly isn't an easy curriculum, but when you're done, you will have a wide variety of skills that are applicable in many different fields. My current job is in a field that I didn't even know existed until I went to Lehigh's career fair.",
    "I also minored in computer science. Even if you don't intend on going into a computer science based career, it is an extremely helpful tool to have in your pocket. I routinely use my computer science skills at my job that is predominantly focused around mechanical engineering.",
    "The CEE department has some of the best professors around, and we get to study some pretty neat stuff. Never gets boring learning about the world around you."
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CarouselSlider.builder(
          itemCount: feedbackList.length,
      itemBuilder: (context, index, ind) =>
           Container(
color: AppColors.lehighBrown01,
            padding:EdgeInsets.all(30),
             child: Container(
             padding:EdgeInsets.all(10),
               decoration:BoxDecoration(
                                color: AppColors.lehighGold01,

                 borderRadius: BorderRadius.all(
                   Radius.circular(10.0)
                 )

               ),


      child:Text(
        feedbackList[index],
        style:TextStyle(fontFamily: "Merriweather",color: AppColors.lehighBrown01, fontSize: 12,  ),textAlign: TextAlign.center)
      ),
             ),
            carouselController: buttonCarouselController,
            options: CarouselOptions(
              enableInfiniteScroll: true,
              autoPlay: true,
              aspectRatio: 1.3,
             viewportFraction:.9,
             // height:10,
              initialPage: 0,


            )
        ),


      ],
    );
  }
}
