import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:untitled/FritzTour.dart';
import 'package:untitled/StepsTour.dart';
import 'package:untitled/WhitakerTour.dart';
import 'package:untitled/campus_map.dart';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:untitled/packardTour.dart';
import 'package:untitled/styles/colors.dart';

class QRViewExample extends StatefulWidget {
  @override
  _QRViewExampleState createState()  => _QRViewExampleState();

}
class _QRViewExampleState extends State<QRViewExample> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? controller;



  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }




@override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(flex: 4, child: _buildQrView(context)),
          Expanded(
            flex: 1,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
 style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Color(0xff502d0e)),
                          ),
                            onPressed: () async {
                              await controller?.toggleFlash();
                              setState(() {});
                            },
                            child: FutureBuilder(
                              future: controller?.getFlashStatus(),
                              builder: (context, snapshot) {
                                if (snapshot.data == true) {
                                  return Icon(Icons.flash_on);
                                }
                                else {
                                  return Icon(Icons.flash_off);
                                }
                              })
                            ),
                      ),

                      Container(
                        margin: EdgeInsets.all(8),
                            child: ElevatedButton(
                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Color(0xff502d0e)),
                          ),
                            onPressed: () async {
                              await controller?.flipCamera();
                              setState(() {});
                            },
             child: Icon(Icons.flip_camera_ios_outlined),


                            ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(

                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Color(0xff502d0e)),
),


                          onPressed: () async {
          Navigator.pop(context, false);
                          },
                          child: Text('Back', style: TextStyle(fontSize: 20)),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('no Permission')),
      );
    }
  }
   Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }



  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      // setState(() {
      controller.pauseCamera();

      if (scanData != null) {
                  print(scanData);

        switch (scanData.code) {
          case "packard":
            {
              Navigator.push(
                  context, MaterialPageRoute(
                  builder: (context) => PackardTour()));
            }
            break;
          case "whitaker":
            {
              Future.delayed(Duration.zero, () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => WhitakerTour()));
              });
            }
            break;
          case "steps":
            {
              Future.delayed(Duration.zero, () {
                Navigator.push(
                    context, MaterialPageRoute(
                    builder: (context) => StepsTour()));
              });
            }
            break;
          case "fritz":
            {
              Future.delayed(Duration.zero, () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FritzTour()));
              });
            }
            break;
        }
      }
    });

  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }


}
