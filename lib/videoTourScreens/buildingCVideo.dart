import 'package:untitled/LandingPages/fritzInfo.dart';
import 'package:untitled/home_screen.dart';
import 'package:untitled/styles/colors.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';
import '../FritzTour.dart';
import '../LandingPages/whitakerInfo.dart';

class BuildingCVideo extends StatefulWidget{
   int index;
   BuildingCVideo(this.index);
  @override
  BuildingCVideoState createState() => BuildingCVideoState();
}

class BuildingCVideoState extends State<BuildingCVideo>{
  late VideoPlayerController _videoController;
        void navigateto(){
           if (widget.index == 5) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => FritzInfo(widget.index)));
              }

              else if (widget.index == 3) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Home()));
              }
              //first page
              else if (widget.index == 2) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Home()));
              }

              if (widget.index == 1) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Home()));
              }
            }
  void initState() {
    super.initState();
    _videoController = VideoPlayerController.asset('assets/BuildingC.mp4')
      ..initialize().then((value) =>
      {
        _videoController.addListener(() { //custom Listner
          setState(() {
            if (!_videoController.value.isPlaying &&
                _videoController.value.isInitialized &&
                (_videoController.value.duration == _videoController.value
                    .position)) {
              print("videofinished");
               if (widget.index == 5) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => FritzInfo(widget.index)));
              }

              else if (widget.index == 3) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Home()));
              }
              //first page
              else if (widget.index == 2) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Home()));
              }

              if (widget.index == 1) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Home()));
              }
            }
            //checking the duration and position every time
            //Video Completed//
            setState(() {});
          });
        })
      });
  }

  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Building C",

          style: TextStyle(fontFamily: "Merriweather", fontSize: 20,fontWeight:FontWeight.w700,color: AppColors.lehighBrown01),
        ),
               elevation:0,
        backgroundColor: Colors.white,
      ),


      body: Center(
child:      _videoController.value.isInitialized
            ? AspectRatio(aspectRatio:_videoController.value.aspectRatio,
        child:VideoPlayer(_videoController),
       ): Container(
          child: ElevatedButton(onPressed: () { navigateto();}, child: Icon(Icons.navigate_next))


),
      ),

      persistentFooterButtons:[
Stack(
  children: <Widget>[
    Align(
alignment:Alignment.bottomLeft,


          child:
              Padding(
                padding:const EdgeInsets.only(left:20.0),
              child:

          FloatingActionButton(


        backgroundColor: AppColors.lehighBrown01,
        onPressed: () {
            setState(() {
              _videoController.value.isPlaying
                  ? _videoController.pause()
                  : _videoController.play();
            });
          },
          child: Icon(
            _videoController.value.isPlaying ? Icons.pause : Icons.play_arrow,
            color: Colors.white,
          ),
        ),
    ),
    ),
  Align(
alignment:Alignment.bottomRight,
            child:
                  Padding(
                padding:const EdgeInsets.only(right:20.0),
              child:
            FloatingActionButton(
        backgroundColor: AppColors.lehighBrown01,
        onPressed: () {
          navigateto();
          },
          child: Icon(Icons.skip_next,
            color: Colors.white,
          ),
        ),
    ),
  ),
    ]
)
      ],








    );
  }

  @override
  void dispose() {
    super.dispose();
    _videoController.dispose();
  }
}
