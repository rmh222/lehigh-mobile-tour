import 'package:untitled/LandingPages/buildingcinfo.dart';
import 'package:untitled/LandingPages/fritzInfo.dart';
import 'package:untitled/home_screen.dart';
import 'package:untitled/styles/colors.dart';
import 'package:video_player/video_player.dart';

import 'package:flutter/material.dart';

import '../FritzTour.dart';
import '../LandingPages/packardInfo.dart';
import '../LandingPages/whitakerInfo.dart';

class WhitakerVideo extends StatefulWidget{
   int index;
   WhitakerVideo(this.index);
  @override
  WhitakerVideoState createState() => WhitakerVideoState();
}

class WhitakerVideoState extends State<WhitakerVideo> {
  void navigateto() {
    //second page
    if (widget.index == 5) {
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => PackardInfo(widget.index)));
    }
    else if (widget.index == 3) {
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => FritzInfo(widget.index)));
    }
    //first page
    else if (widget.index == 2) {
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => WhitakerInfo(widget.index)));
    }

    if (widget.index == 1) {
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => BuildingCInfo(widget.index)));
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Fritz Lab",
            style: TextStyle(fontFamily: "Merriweather",
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: AppColors.lehighBrown01),
          ),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
               body: Center(
child:Stack(
  children: <Widget>[
  Align(
alignment:Alignment.bottomCenter,
            child:
                  Padding(
                padding:const EdgeInsets.only(right:20.0),
              child:
            FloatingActionButton(
        backgroundColor: AppColors.lehighBrown01,
        onPressed: () {
          navigateto();
          },
          child: Icon(Icons.skip_next,
            color: Colors.white,
          ),
        ),
    ),
  ),
    ]
    ),
        )


    );
  }
}