import 'package:untitled/LandingPages/buildingcinfo.dart';
import 'package:untitled/LandingPages/fritzInfo.dart';
import 'package:untitled/home_screen.dart';
import 'package:untitled/styles/colors.dart';
import 'package:video_player/video_player.dart';

import 'package:flutter/material.dart';

import '../FritzTour.dart';
import '../LandingPages/packardInfo.dart';
import '../LandingPages/whitakerInfo.dart';

class PackardVideo extends StatefulWidget{
   int index;
   PackardVideo(this.index);
  @override
  PackardVideoState createState() => PackardVideoState();
}

class PackardVideoState extends State<PackardVideo>{


  late VideoPlayerController _videoController;

  void initState() {
    super.initState();
    _videoController = VideoPlayerController.asset('assets/PackardLab.mp4')
      ..initialize().then((value) =>
      {
        _videoController.addListener(() { //custom Listner
          setState(() {
            if (!_videoController.value.isPlaying &&
                _videoController.value.isInitialized &&
                (_videoController.value.duration == _videoController.value
                    .position)) {
              print("videofinished");
              if (widget.index == 5) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Home()));
              }

              else if (widget.index == 3) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => BuildingCInfo(widget.index)));
              }
              //first page
              else if (widget.index == 2) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => BuildingCInfo(widget.index)));
              }

              if (widget.index == 1) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => FritzInfo(widget.index)));
              }
            }
            //checking the duration and position every time
            //Video Completed//
            setState(() {});
          });
        })
      });
  }
  void navigateto(){
     if (widget.index == 5) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Home()));
              }

              else if (widget.index == 3) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => BuildingCInfo(widget.index)));
              }
              //first page
              else if (widget.index == 2) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => BuildingCInfo(widget.index)));
              }

              if (widget.index == 1) {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => FritzInfo(widget.index)));
              }
            }
  

  Widget build(BuildContext context){
    return Scaffold(
     appBar: AppBar(
        title: Text("Packard Lab",
          style: TextStyle(fontFamily: "Merriweather", fontSize: 20,fontWeight:FontWeight.w700,color: AppColors.lehighBrown01),
        ),
               elevation:0,
        backgroundColor: Colors.white,
      ),
      body: Center(
        child:_videoController.value.isInitialized ? AspectRatio(aspectRatio:_videoController.value.aspectRatio,
        child:VideoPlayer(_videoController),
        ): Container(  child: ElevatedButton(
                onPressed: () {
                  navigateto();
                }, child: null,),
      ),
      ),


      persistentFooterButtons:[
Stack(
  children: <Widget>[
    Align(
alignment:Alignment.bottomLeft,


          child:
              Padding(
                padding:const EdgeInsets.only(left:20.0),
              child:

          FloatingActionButton(


        backgroundColor: AppColors.lehighBrown01,
        onPressed: () {
            setState(() {
              _videoController.value.isPlaying
                  ? _videoController.pause()
                  : _videoController.play();
            });
          },
          child: Icon(
            _videoController.value.isPlaying ? Icons.pause : Icons.play_arrow,
            color: Colors.white,
          ),
        ),
    ),
    ),
  Align(
alignment:Alignment.bottomRight,
            child:
                  Padding(
                padding:const EdgeInsets.only(right:20.0),
              child:
            FloatingActionButton(
        backgroundColor: AppColors.lehighBrown01,
        onPressed: () {
          navigateto();
          },
          child: Icon(Icons.skip_next,
            color: Colors.white,
          ),
        ),
    ),
  ),
    ]
)
      ],









    );
  }

  @override
  void dispose() {
    super.dispose();
    _videoController.dispose();
  }
}
