import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'TourOptions.dart';
import 'styles/colors.dart';
import 'dart:ui';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:untitled/campus_map.dart';
import 'nav_bar.dart';


class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home>  with TickerProviderStateMixin {
  late AnimationController controller;
  static const LatLng PACKARD_LOCATION = LatLng(40.607801, -75.379196);
  static const LatLng FRITZ_LOCATION = LatLng(40.6080175, -75.3761864);
  static const LatLng WHITAKER_LOCATION = LatLng(40.6089908, -75.3763878);
  static const LatLng BUILDING_C_LOCATION = LatLng(40.600977, -75.362498);
  int _selectedIndex = 0;
  PageController pageController = PageController();
  String? closestBuilding;

  void onPageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  Position? _currentPosition;
  int _currentIndex = 0;
  final _inactiveColor = Colors.grey;


  final List<String> feedbackList = [
    "I love having the freedom of choosing which part of computer science I want to go into.",
    "I think it's neat to learn how all of our modern electronics work, and use that knowledge to create something that seems really impressive on my own"
    ,
    "Mechanical engineering is a very diverse and hands-on field. If you like building things and using your skills to come up with clever solutions to problems, you will enjoy mechanical engineering. It certainly isn't an easy curriculum, but when you're done, you will have a wide variety of skills that are applicable in many different fields. My current job is in a field that I didn't even know existed until I went to Lehigh's career fair.",
    "I also minored in computer science. Even if you don't intend on going into a computer science based career, it is an extremely helpful tool to have in your pocket. I routinely use my computer science skills at my job that is predominantly focused around mechanical engineering.",
    "The CEE department has some of the best professors around, and we get to study some pretty neat stuff. Never gets boring learning about the world around you."
  ];


 _getCurrentLocation()  {
    Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
       .then((Position position) async {
     setState(() {
       _currentPosition = position;
       print('CURRENT POS: $_currentPosition');
     });
   });
 }

 Widget getBody(){

     List<Widget> pages = [TourOption(closestBuilding!),Map()
];
      return IndexedStack(
    index: _currentIndex,
    children: pages,
  );
 }

 @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
        setState(() {});
      });
            super.initState();
                _getCurrentLocation();


  }

  String getClosestBuilding() {
    double packard = Geolocator.distanceBetween(_currentPosition!.latitude, _currentPosition!.longitude, PACKARD_LOCATION.latitude, PACKARD_LOCATION.longitude);
    double fritz =  Geolocator.distanceBetween(_currentPosition!.latitude, _currentPosition!.longitude, FRITZ_LOCATION.latitude, FRITZ_LOCATION.longitude);
    double whitaker = Geolocator.distanceBetween(_currentPosition!.latitude, _currentPosition!.longitude, WHITAKER_LOCATION.latitude, WHITAKER_LOCATION.longitude);
    double building_c =  Geolocator.distanceBetween(_currentPosition!.latitude,_currentPosition!.longitude , BUILDING_C_LOCATION.latitude, BUILDING_C_LOCATION.longitude);
   final map = <double, String>{};
    final x = {packard: "packard", fritz: "fritz",
    whitaker: "whitaker",  building_c: "building_c"};
map.addAll(x);
double min = x.keys.first;
String buildingMin = x.values.first;
map.forEach((key, value) {
  if(key < min){
    min = key;
    buildingMin = value ;

  }
});
return buildingMin;
 }

  @override
  Widget build(BuildContext context) {

   var size = MediaQuery.of(context).size;
    return Scaffold(

   body:
   Center(
     child:

    getBody(),
   ),

      bottomNavigationBar: _buildBottomBar(),

    );
  }
    Widget _buildBottomBar(){
    return CustomAnimatedBottomBar(
      containerHeight: 70,
      backgroundColor: Colors.white,
      selectedIndex: _currentIndex,
      showElevation: true,
      itemCornerRadius: 24,
      curve: Curves.easeIn,
      onItemSelected: (index) => setState(() => _currentIndex = index),
      items: <BottomNavyBarItem>[
        BottomNavyBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
          activeColor:  AppColors.lehighBrown01,
          inactiveColor: _inactiveColor,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.map_outlined),
          title: Text('Map'),
          activeColor: AppColors.lehighBrown01,
          inactiveColor: _inactiveColor,
          textAlign: TextAlign.center,
        ),

      ],
    );
  }


}
