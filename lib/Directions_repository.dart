import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:dio/dio.dart';
import 'Directions.dart';
class DirectionsRepository{
    static const String baseUrl = "https://maps.googleapis.com/maps/api/directions/json?";
  static const double CAMERA_ZOOM = 15;
  static const double CAMERA_TILT = 0;
  static const double CAMERA_BEARING = 30;
  static const LatLng PACKARD_LOCATION = LatLng(40.607012, -75.377736);
  static const LatLng FRITZ_LOCATION = LatLng(40.6080175, -75.3761864);
  static const LatLng WHITAKER_LOCATION = LatLng(40.6089908, -75.3763878);
  static const LatLng STEPS_LOCATION = LatLng(40.6083326, -75.3790104);
  final Dio _dio;
  DirectionsRepository({Dio? dio}) : _dio = dio ?? Dio();
  Future<Directions?> getDirections({required
  LatLng origin,
    required LatLng destination}) async {
    final res = await _dio.get(baseUrl,

        queryParameters: {
          'origin': '${origin.latitude}, ${origin
              .longitude}',
          'destination': '${PACKARD_LOCATION.latitude}, ${PACKARD_LOCATION
              .longitude}',
          'key':  'AIzaSyAYWjfg6nDnCHvorZhEkgEl01iEN99jT1g',

        });

    print('res ');
    print(res.statusCode);
    if (res.statusCode == 200) {
      return Directions.fromMap(res.data);
    }

    return null;
  }

}