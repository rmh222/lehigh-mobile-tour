import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/LandingPages/fritzInfo.dart';
import 'package:untitled/LandingPages/packardInfo.dart';
import 'package:untitled/LandingPages/whitakerInfo.dart';
import 'LandingPages/buildingcinfo.dart';
import 'styles/colors.dart';
import 'utils/values.dart';
import 'dart:ui';
import 'package:geolocator/geolocator.dart';

class TourOption extends StatelessWidget {
  TourOption(this.closestBuilding);
  final String closestBuilding;
  // Position? _currentPosition;

  // String getClosestBuilding() {
  //   double packard = Geolocator.distanceBetween(
  //       _currentPosition!.latitude, _currentPosition!.longitude,
  //       BuildingLocations.PACKARD_LOCATION.latitude, BuildingLocations.PACKARD_LOCATION.longitude);
  //   double fritz = Geolocator.distanceBetween(
  //       _currentPosition!.latitude, _currentPosition!.longitude,
  //       BuildingLocations.FRITZ_LOCATION.latitude, BuildingLocations.FRITZ_LOCATION.longitude);
  //   double whitaker = Geolocator.distanceBetween(
  //       _currentPosition!.latitude, _currentPosition!.longitude,
  //       BuildingLocations.WHITAKER_LOCATION.latitude, BuildingLocations.WHITAKER_LOCATION.longitude);
  //   double building_c = Geolocator.distanceBetween(
  //       _currentPosition!.latitude, _currentPosition!.longitude,
  //      BuildingLocations.BUILDING_C_LOCATION.latitude, BuildingLocations.BUILDING_C_LOCATION.longitude);
  //
  //   final map = <double, String>{};
  //   final x = {packard: "packard", fritz: "fritz",
  //     whitaker: "whitaker", building_c: "building_c"};
  //   map.addAll(x);
  //   double min = x.keys.first;
  //   String buildingMin = x.values.first;
  //   map.forEach((key, value) {
  //     if (key < min) {
  //       min = key;
  //       buildingMin = value;
  //     }
  //   });
  //   print("building min ");
  //   print(buildingMin);
  //   return buildingMin;
  // }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        elevation:0,
        backgroundColor: Colors.white,
        title: Text("Choose a Tour",
          style: TextStyle(fontFamily: "Merriweather", fontSize: 20,fontWeight:FontWeight.w700,color: AppColors.lehighBrown01),),
      ),
      body: Center(
        child:
        Container(
          child:
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new GestureDetector(
                onTap: () {
                  if (closestBuilding == "packard") {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => PackardInfo(1)));
                  }
                  else if (closestBuilding == "fritz") {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => FritzInfo(2)));
                  }
                  else if (closestBuilding == "whitaker") {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => WhitakerInfo(3)));
                  }
                  else if (closestBuilding == "building_c") {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => BuildingCInfo(5)));
                  }
                }

                , child:
              Container(
                  padding: EdgeInsets.all(5),

                  // alignment: Alignment.center,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height * .30,
                  decoration: BoxDecoration(


                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(5),
                    // color: Color(0xff502d0e),

                    image: DecorationImage(
                      image: AssetImage('assets/walking_around_campus.jpg'),
                      fit: BoxFit.fill,
                      colorFilter: ColorFilter.mode(
                          Color(0xff502d0e).withOpacity(0.5),
                          BlendMode.srcATop),
                    ),),
                  child:
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,

                      // crossAxisAlignment: CrossAxisAlignment.center,
                      //  mainAxisAlignment: MainAxisAlignment.start,
                      //  mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Text("Self-Guided Tour", style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontFamily: "Merriweather",)),


                      ]
                  )

              ),


              ),
              SizedBox(
                height:30,
              ),


              new GestureDetector(
                onTap: () {
                  print("clicked 1");
                },
                child:
                Container(
                    padding: EdgeInsets.all(5),
                    // alignment: Alignment.center,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height * .30,
                    decoration: BoxDecoration(

                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(5),
                      // color: Color(0xff502d0e),

                      image: DecorationImage(
                        image: AssetImage('assets/lehigh-mobile-app.jpg'),
                        fit: BoxFit.fill,
                        colorFilter: ColorFilter.mode(
                            Color(0xff502d0e).withOpacity(0.5),
                            BlendMode.srcATop),
                      ),),
                    child:
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,

                        // crossAxisAlignment: CrossAxisAlignment.center,
                        //  mainAxisAlignment: MainAxisAlignment.start,
                        //  mainAxisSize: MainAxisSize.max,
                        children: <Widget>[


                          Text("Virtual Tour", style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontFamily: "Merriweather",)),


                        ]
                    )

                ),


              ),

              //
              // Container(
              //   child: Carousel(),


            ],

          ),
        ),
      )

    );
  }
}