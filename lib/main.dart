import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/home_screen.dart';

Future main() async {
  runApp(
    MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mobile Tour',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  void didChangeDependencies() {
    precacheImage(AssetImage('assets/packard2.jpg'), context);
    precacheImage(AssetImage('assets/packard.jpg'), context);
    precacheImage(AssetImage('assets/walking_around_campus.jpg'), context);
    precacheImage(AssetImage('assets/lehigh-mobile-app.jpg.jpg'), context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/packard.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            Container(
              height:30,
              color: Colors.white.withOpacity(.8),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topRight: Radius.zero, topLeft: Radius.zero, bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20), ),
              color: Colors.white.withOpacity(.8),
      ),
              child: ListTile(
                title: Text("Lehigh University", style: TextStyle(fontFamily: 'Merriweather', color: Color(0xff502d0e), fontWeight: FontWeight.w600)),
                subtitle: Text("Engineering Mobile Tour", style: TextStyle(fontFamily: "Merriweather", color: Color(0xff502d0e), fontWeight:FontWeight.w500)),
              ),
            ),

             SizedBox(
              height: MediaQuery.of(context).size.height*.6
            ),

            Container(
                       child: InkWell(
                    onTap: () {
    Navigator.pushReplacement(
    context, MaterialPageRoute(
    builder: (context) => Home()));
    },

               child: Container(
                 alignment: Alignment.center,
                      height:MediaQuery.of(context).size.height*.07,
                  width: MediaQuery.of(context).size.width *.3,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                                      color: Color(0xff502d0e),
              ),
                 child: Text("START", style: TextStyle(color: Colors.white,  fontFamily: "Merriweather", fontSize: 20,  fontWeight: FontWeight.w400 ), textAlign: TextAlign.center,),




                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
