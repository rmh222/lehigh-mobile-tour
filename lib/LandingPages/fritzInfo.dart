import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/qr_scan.dart';

import '../navigation/navigate_fritz.dart';

class FritzInfo extends StatefulWidget {
   final int index;
  const FritzInfo(this.index);
  @override
  FritzInfoState createState() => FritzInfoState();
}

class FritzInfoState extends State<FritzInfo> {
  @override
  Widget build(BuildContext context){
    return Scaffold(
       body: Container(
          decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/fritzlab.jpg'),
            fit: BoxFit.cover,
          ),

          ),
         child:
         Center(

         child: Column(
           children: [
             SizedBox(
               height: 30
             ),
             Container(
               width: MediaQuery.of(context).size.width,

               color: Colors.white,
                child:
                Text("Fritz", style: TextStyle(fontFamily: 'Merriweather', color: Color(0xff502d0e), fontSize: 30, fontWeight: FontWeight.w400), textAlign: TextAlign.center,),
             ),
              SizedBox(
                height:MediaQuery.of(context).size.height*.5
              ),
              Container(
                       child: InkWell(
                    onTap: () {
    Navigator.push(
   context, MaterialPageRoute(
   builder: (context) => NavigateFritz()));
    },

               child: Container(
                 alignment: Alignment.center,
                      height:MediaQuery.of(context).size.height*.07,
                  width: MediaQuery.of(context).size.width *.4,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                                      color: Color(0xff502d0e),
              ),
                 child: Text("NAVIGATE", style: TextStyle(color: Colors.white,  fontFamily: "Merriweather" , fontSize: 15, fontWeight: FontWeight.w400), textAlign: TextAlign.center,),




                ),
       ),
             ),
            SizedBox(
              height:40
            ),

            Container(
                       child: InkWell(
                    onTap: () {
    Navigator.push(
    context, MaterialPageRoute(
    builder: (context) => QRViewExample()));
    },

               child: Container(
                 alignment: Alignment.center,
                      height:MediaQuery.of(context).size.height*.07,
                  width: MediaQuery.of(context).size.width *.4,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                                      color: Color(0xff502d0e),
              ),
                 child: Text("SCAN QR CODE", style: TextStyle(color: Colors.white,  fontFamily: "Merriweather" , fontSize:15), textAlign: TextAlign.center,),




                ),
              ),
            ),


         ],
         ),
    ),
       ),
);




  }
}