import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/qr_scan.dart';
import 'package:untitled/styles/colors.dart';
import 'package:untitled/navigation/navigate_buildingc.dart';
import 'package:untitled/videoTourScreens/buildingCVideo.dart';
import '../nav_bar.dart';
import '../navigation/navigate_packard.dart';

class BuildingCInfo extends StatefulWidget {
    final int index;
  const BuildingCInfo(this.index);
  @override
  BuildingCInfoState createState() => BuildingCInfoState();
}

class BuildingCInfoState extends State<BuildingCInfo> {
  int _currentIndex = 0;
  final _inactiveColor = Colors.grey;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
body:

         new Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/buildingc-4.jpg'),
            fit: BoxFit.cover,
      ),
    ),


        child:
        Center(
          child: Column(
            children: [
              SizedBox(
                  height: 30
              ),
              Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,

                color: Colors.white,
                child:
                Text("Building C", style: TextStyle(fontFamily: 'Merriweather',
                    color: Color(0xff502d0e),
                    fontSize: 30,
                    fontWeight: FontWeight.w400), textAlign: TextAlign.center,),
              ),
              SizedBox(
                  height: MediaQuery
                      .of(context)
                      .size
                      .height * .5
              ),
              Container(
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context, MaterialPageRoute(
                        builder: (context) => NavigateBuildingC()));
                  },

                  child: Container(
                    alignment: Alignment.center,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height * .07,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width * .4,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xff502d0e),
                    ),
                    child: Text("NAVIGATE", style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Merriweather",
                        fontSize: 15,
                        fontWeight: FontWeight.w400),
                      textAlign: TextAlign.center,),


                  ),
                ),
              ),
              SizedBox(
                  height: 40
              ),

              Container(
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context, MaterialPageRoute(
                        builder: (context) => QRViewExample()));
                  },

                  child: Container(
                    alignment: Alignment.center,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height * .07,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width * .4,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xff502d0e),
                    ),
                    child: Text("SCAN QR CODE", style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Merriweather",
                        fontSize: 15), textAlign: TextAlign.center,),


                  ),
                ),
              ),
              Container(
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context, MaterialPageRoute(
                        builder: (context) => BuildingCVideo(widget.index)));
                  },
                  // child: Container(
                  //   color: Colors.pink,
                  //   height: MediaQuery
                  //       .of(context)
                  //       .size
                  //       .height * .07,
                  //   width: MediaQuery
                  //       .of(context)
                  //       .size
                  //       .width * .4,
                  // ),
                ),
              ),


            ],
          ),
        ),
         ),

    );

  }


  }


