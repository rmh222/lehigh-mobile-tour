import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/qr_scan.dart';
import 'package:untitled/videoTourScreens/PackardVideo.dart';

import '../navigation/navigate_packard.dart';

class PackardInfo extends StatefulWidget {
  final int index;
  const PackardInfo(this.index);
  @override
  PackardInfoState createState() => PackardInfoState();
}

class PackardInfoState extends State<PackardInfo> {

  @override
  Widget build(BuildContext context){
    return Scaffold(
       body: Container(
          decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/packard2.jpg'),
            fit: BoxFit.cover,
          ),

          ),
         child:
         Center(

         child: Column(
           children: [
             SizedBox(
               height: 30
             ),
             Container(
               width: MediaQuery.of(context).size.width,

               color: Colors.white,
                child:
                Text("Packard Laboratory", style: TextStyle(fontFamily: 'Merriweather', color: Color(0xff502d0e), fontSize: 30, fontWeight: FontWeight.w400), textAlign: TextAlign.center,),
             ),
              SizedBox(
                height:MediaQuery.of(context).size.height*.5
              ),
              Container(
                       child: InkWell(
                    onTap: () {
 Navigator.push(
   context, MaterialPageRoute(
  builder: (context) => NavigatePackard()));
    },

               child: Container(
                 alignment: Alignment.center,
                      height:MediaQuery.of(context).size.height*.07,
                  width: MediaQuery.of(context).size.width *.4,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                                      color: Color(0xff502d0e),
              ),
                 child: Text("NAVIGATE", style: TextStyle(color: Colors.white,  fontFamily: "Merriweather" , fontSize: 15, fontWeight: FontWeight.w400), textAlign: TextAlign.center,),




                ),
       ),
             ),
            SizedBox(
              height:40
            ),

            Container(
                       child: InkWell(
                    onTap: () {
    Navigator.push(
    context, MaterialPageRoute(
    builder: (context) => QRViewExample()));
    },

               child: Container(
                 alignment: Alignment.center,
                      height:MediaQuery.of(context).size.height*.07,
                  width: MediaQuery.of(context).size.width *.4,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                                      color: Color(0xff502d0e),
              ),
                 child: Text("SCAN QR CODE", style: TextStyle(color: Colors.white,  fontFamily: "Merriweather" , fontSize:15), textAlign: TextAlign.center,),




                ),
              ),
            ),
               Container(
                       child: InkWell(
                    onTap: () {
    Navigator.push(
    context, MaterialPageRoute(
    builder: (context) => PackardVideo(widget.index)));
    },
child:Container(
    color: Colors.pink,
     height:MediaQuery.of(context).size.height*.07,
                  width: MediaQuery.of(context).size.width *.4,
),
    ),
               ),


         ],
         ),
    ),
       ),
);




  }
}