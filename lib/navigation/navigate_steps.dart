import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:untitled/Directions.dart';
import 'package:untitled/styles/colors.dart';

import '../Directions_repository.dart';

class NavigateSteps extends StatefulWidget {

  @override
  NavigateStepsState createState() => NavigateStepsState();
}

class NavigateStepsState extends State<NavigateSteps> {
//String googleApiKey = "**"
  static const String baseUrl = "https://maps.googleapis.com/maps/api/directions/json?";
  static const double CAMERA_ZOOM = 15;
  static const double CAMERA_TILT = 0;
  static const double CAMERA_BEARING = 30;

  static const LatLng STEPS_LOCATION = LatLng(40.6083326, -75.3790104);

  final lines = <Polyline>[];

  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = {};
  late Position _currentPosition;
  Marker? _origin;
  Marker? _destination;

  Directions? _info;

  Set<Polyline> _polylines = Set<Polyline>();
  List<LatLng> polylineCoordinates = [];
  late PolylinePoints polylinePoints;

  List<LatLng>? routeCoords;

  @override
  void initState() {
    super.initState();

    _getCurrentLocation();

  }


  CameraPosition initialLocation = CameraPosition(
      zoom: CAMERA_ZOOM,
      bearing: CAMERA_BEARING,
      tilt: CAMERA_TILT,
      target: STEPS_LOCATION
  );


  _getCurrentLocation() async {
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) async {
      setState(() {
        _currentPosition = position;
        print('CURRENT POS: $_currentPosition');
      });
    });
                polylinePoints = PolylinePoints();

  }



  void showPinsOnMap() {
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId('sourcePin'),
        position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
       // icon: sourceIcon,
        onTap: () {
          setState(() {
         //   this.userBadgeSelected = true;
          });
        }
      ));

      _markers.add(Marker(
        markerId: MarkerId('destinationPin'),
        position: STEPS_LOCATION,
        //icon: ,
        onTap: () {
          setState(() {
           //this.pinPillPosition = PIN_VISIBLE_POSITION;
          });
        }
      ));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
          title: Text("Navigate to Steps",
          style: TextStyle(fontFamily: "Merriweather", fontSize: 20,fontWeight:FontWeight.w700,color: AppColors.lehighBrown01),),
      ),
        body: Stack(
            alignment: Alignment.center,
            children: [
              GoogleMap(
                markers: _markers,
                polylines: _polylines,
                myLocationEnabled: true,

                myLocationButtonEnabled: false,
                zoomControlsEnabled: false,

                initialCameraPosition: initialLocation,
 onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
                showPinsOnMap();

                setPolylines();
              },

              )
            ])
    );
  }

  void setPolylines() async {
      PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
    //googleApiKey,
      PointLatLng(
        _currentPosition.latitude,
        _currentPosition.longitude
      ),
      PointLatLng(
        STEPS_LOCATION.latitude,
        STEPS_LOCATION.longitude
      ) ,
      travelMode: TravelMode.walking
    );

    if (result.status == 'OK') {
     print('the result is ok ' );
     print(result);
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });

      setState(() {
        _polylines.add(
          Polyline(
            width: 10,
            polylineId: PolylineId('polyLine'),
            color: Color(0xFF08A5CB),
            points: polylineCoordinates
          )
        );
      });
    }
    else{
      print(result.status);
    }
  }

  void _addMarker(LatLng pos) async {
    if (_origin == null || (_origin != null && _destination != null)) {
      // Origin is not set OR Origin/Destination are both set
      // Set origin
      setState(() {
        _origin = Marker(
          markerId: const MarkerId('origin'),
          infoWindow: const InfoWindow(title: 'Origin'),
          icon:
          BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          position: pos,
        );
        // Reset destination
        _destination = null;

        // Reset info
        _info = null;
      });
    } else {
      // Origin is already set
      // Set destination
      setState(() {
        _destination = Marker(
          markerId: const MarkerId('destination'),
          infoWindow: const InfoWindow(title: 'Destination'),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          position: pos,
        );
      });

      // Get directions
      final directions = await DirectionsRepository()
          .getDirections(origin: LatLng(_currentPosition.latitude, _currentPosition.longitude), destination: STEPS_LOCATION);
      setState(() => _info = directions);
    }
  }
}

