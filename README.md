# Lehigh Mobile Tour

## API Setup:

**These steps are required before running the app for the first time locally. This app uses Google Map services.**
Get your API keys [here is a guide](https://developers.google.com/maps/documentation/android-sdk/get-api-key). Enable Maps SDK for Android, Maps SDK for iOS, and Directions for your API.

1) In android/app/src/main/AndroidManifest.xml add the placeholder like so (if not already there):

    ```
    <meta-data android:name="com.google.android.geo.API_KEY"
    android:value="***"/>
    ```

2) In ios/Runner/AppDelegate.swift insert API key in this line 
    ```
    GMSServices.provideAPIKey("***")
    ```

In the lib folder you will also have to put this key in as a string where I have commented out String googleApiKey = "**". 


## To Run:

1. Clone this repository / branch off the master

2. Open in Android Studio (my preference for mobile app development) or Visual Studio.

Before Running on a Device / Simulator:

`npm install`

## To Run on a Device / Simulator

To run on a physical iOS device, Xcode is required. Code signing before running may be required.

Android Simulators can be downloaded through Android Studio.

iOS simulators can be downloaded through Xcode

**Further modifications may be needed, when cloning the repository for the first time.**

## Organization:

`/lib`

`/assets`

`/ios`

`/android`

`/assets`

`pubspec.yaml`

`/assets`: Holds contents such as images, fonts, and video files. These assets have to be referenced under the pubspec.yaml file. This is
how flutter recognizes where the assets are present.

`/lib`: Holds all the user-written code needed for the app.

`/lib/styles`: Holds file to define app colors, customized styles in app.

`/lib/LandingPages`: Holds landing pages of buildings which displays
buttons **navigation** and **QR Code Scan** to start the building tour.

`/lib/navigation`: Highlighed navigation routes from user location to building.

`/lib/videoTourScreens`: Holds the video tour pages for each building
